package com.example.redsocial;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Region;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.redsocial.Api.Api;
import com.example.redsocial.Api.Servicios.ServicioPeticion;
import com.example.redsocial.ViewModels.Detalles_Usuarios;
import com.example.redsocial.ViewModels.Peticion_Login;
import com.example.redsocial.ViewModels.Todos_Usuarios;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {

    //Creando Variables
    private Button btncerrarsesion;
    private ListView listausuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //Enlazando variables a controles de interfaz
        btncerrarsesion = findViewById(R.id.BtnCerrar);
        listausuarios = findViewById(R.id.ListVUsuarios);

        //Boton para cerrar sesión
        btncerrarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferencias.edit();
                //Sobreescribir el archivo con un token vacío para evitar acceso a la app
                editor.putString("TOKEN", "");
                editor.apply();
                //Se manda de nuevo a la pantalla de Login
                startActivity(new Intent(Menu.this, Login.class));
            }
        });
        //Servicio Petición para mostrar todos los usuarios
        ServicioPeticion service = Api.getApi(Menu.this).create(ServicioPeticion.class);
        Call<Todos_Usuarios> usuariosCall = service.obtenerUsuarios();
        usuariosCall.enqueue(new Callback<Todos_Usuarios>() {
            @Override
            public void onResponse(Call<Todos_Usuarios> call, Response<Todos_Usuarios> response) {
                Todos_Usuarios peticion = response.body();
                //If en caso de problemas de petición con el servidor
                if (peticion.estado.equals(null)) {
                    Toast.makeText(Menu.this, "Ha ocurrido un Error, Inténtalo más Tarde", Toast.LENGTH_LONG).show();
                }
                //Obtener los usuarios si es estado regresa un true
                if (peticion.estado.equals("true")) {
                    //Creando una Lista de arreglos de tipo String e Integer para usuarios e id
                    ArrayList<String> Nombres = new ArrayList<String>();
                    final ArrayList<Integer> IdUsuario = new ArrayList<Integer>();
                    //Ciclo que recorre y obtiene todos los registros hasta que llegue al último (-1 para que no se salga del tamaño)
                    for (int i = 0; i < peticion.usuarios.size() - 1; i++) {
                        Nombres.add(peticion.usuarios.get(i).username);
                        IdUsuario.add(peticion.usuarios.get(i).id);
                    }
                    //Llenando el ListView con los datos recuperados
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(Menu.this, android.R.layout.simple_list_item_1, Nombres);
                    listausuarios.setAdapter(adapter);
                    //<-------------------------------------------------------------------------------------------------------------------->
                    // Adaptador que muestra la información cuando se de click en un item ó usuario del listview, su posicion e id
                    listausuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        //Metodo cuando se da click en un item de la lista
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ServicioPeticion service = Api.getApi(Menu.this).create(ServicioPeticion.class);
                            //Regresando un elemento de una lista como una variable acorde a la posicion e id
                            Call<Detalles_Usuarios> detallesCall = service.detallesUsuarios(IdUsuario.get(position));
                            detallesCall.enqueue(new Callback<Detalles_Usuarios>() {
                                @Override
                                public void onResponse(Call<Detalles_Usuarios> call, Response<Detalles_Usuarios> response) {
                                    Detalles_Usuarios peticion = response.body();
                                    if (peticion.estado.equals(null)) {
                                        Toast.makeText(Menu.this, "Ha ocurrido un Error, Inténtalo más Tarde", Toast.LENGTH_LONG).show();
                                    }
                                    if (peticion.estado.equals("true")) {
                                        //Mostrando Detalles del Usuario, id y usuario
                                        Toast.makeText(Menu.this, peticion.usuarioId, Toast.LENGTH_LONG).show();
                                        Toast.makeText(Menu.this, peticion.usuario, Toast.LENGTH_LONG).show();
                                        Toast.makeText(Menu.this, peticion.estado, Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(Menu.this, "No se pudo Conectar", Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Detalles_Usuarios> call, Throwable t) {
                                    Toast.makeText(Menu.this, "Error :(", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                    //<-------------------------------------------------------------------------------------------------------------------->
                } else {
                    Toast.makeText(Menu.this, peticion.estado, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Todos_Usuarios> call, Throwable t) {
                Toast.makeText(Menu.this, "No se pudo Conectar :(", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
