package com.example.redsocial.ViewModels;

import java.util.List;

//Modelo para mostrar a todos los usuario con el estado y la lista de los Usuarios_Data en donde se almacenan todos
//los datos o elementos del arreglo
public class Todos_Usuarios {
    public String estado;
    public List<Usuarios_Data> usuarios;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Usuarios_Data> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuarios_Data> usuarios) {
        this.usuarios = usuarios;
    }
}
