package com.example.redsocial.Api.Servicios;

import com.example.redsocial.ViewModels.Detalles_Usuarios;
import com.example.redsocial.ViewModels.Peticion_Login;
import com.example.redsocial.ViewModels.Registro_Usuario;
import com.example.redsocial.ViewModels.Todos_Usuarios;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @POST("api/todosUsuarios")
    Call<Todos_Usuarios> obtenerUsuarios();

    @FormUrlEncoded
    @POST("api/Detalles_Usuarios")
    Call<Detalles_Usuarios> detallesUsuarios(@Field("usuarioId") int usuarioId);
}
