package com.example.redsocial;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.redsocial.Api.Api;
import com.example.redsocial.Api.Servicios.ServicioPeticion;
import com.example.redsocial.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    //Creando variables para enlazar
    private EditText txtCorreo, txtContrasenia, txtConfirmar;
    private Button Btn_Registrarse;
    private TextView txtIniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //Enlazando variables con controles de interfaz
        txtCorreo = findViewById(R.id.edtTxtCorreoR);
        txtContrasenia = findViewById(R.id.edtTxtContraR);
        txtConfirmar = findViewById(R.id.edtTxtConfirmR);
        txtIniciar = findViewById(R.id.txtVIniciarSesion);
        Btn_Registrarse = findViewById(R.id.BtnRegistoR);

        Btn_Registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!txtContrasenia.getText().toString().equals(txtConfirmar.getText().toString())) {
                    Toast.makeText(Registro.this, "Las Contraseñas Deben ser Iguales", Toast.LENGTH_SHORT).show();
                } else {
                    ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
                    Call<Registro_Usuario> registrarCall = service.registrarUsuario(txtCorreo.getText().toString(), txtContrasenia.getText().toString());
                    registrarCall.enqueue(new Callback<Registro_Usuario>() {
                        @Override
                        public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                            Registro_Usuario peticion = response.body();
                            if (response.body() == null) {
                                Toast.makeText(Registro.this, "Ocurrio un Error, inténtalo más tarde", Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (peticion.estado == "true") {
                                startActivity(new Intent(Registro.this, Login.class));
                                Toast.makeText(Registro.this, "Datos Registrados", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(Registro.this, peticion.detalle, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                            Toast.makeText(Registro.this, "Error :(", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        txtIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Registro.this, Login.class));
            }
        });

    }
}
